package com.example.calltest;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyService extends Service {
    @BindView(R.id.textView)
    TextView txtText;

    WindowManager.LayoutParams params;
    private WindowManager wm;
    View mView;
    public static int nMode;

    LayoutInflater inflate;


    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       int flag;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }
        else {
            flag = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        }
        inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        mView = inflate.inflate(R.layout.pop_layout,null);
        params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                flag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        |WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        |WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.TOP;

        Button button = mView.findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDestroy();
            }
        });

        wm.addView(mView,params);
        if (nMode == 0)     nMode = 1;
        else nMode = 0;
        mView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        if (mView != null)
                            wm.updateViewLayout(mView, params);
                        return true;
                }
                return false;
            }
        });
        txtText=mView.findViewById(R.id.textView);

        Mythread mythread = new Mythread();
        mythread.start();





        return super.onStartCommand(intent, flags, startId);
    }
    int count=0;
    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);


            ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfoList = am.getRunningAppProcesses();
            List<String> processNameList = new ArrayList<String>();

            for (ActivityManager.RunningAppProcessInfo rapi : runningAppProcessInfoList) {
                processNameList.add(rapi.processName);
            }

            StringBuilder processNamesBuilder = new StringBuilder();
            for (String processName : processNameList) {
                processNamesBuilder.append(processName);
                processNamesBuilder.append('\n');
            }
            count ++;


               txtText.setText(processNamesBuilder.toString()+count);

        }
    };

    class Mythread extends Thread{

        @Override
        public void run() {
            while(true){
                Message message = new Message();
                message.arg1=0;
                handler.sendMessage(message);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(wm != null)
        {
            if(mView != null){
                wm.removeView(mView);
                mView = null;
            }
            wm = null;
        }
    }


}



